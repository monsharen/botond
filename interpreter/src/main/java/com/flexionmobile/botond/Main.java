package com.flexionmobile.botond;

import java.awt.*;
import java.io.File;
import java.util.Arrays;
import java.util.List;

import com.flexionmobile.botond.command.Command;
import com.flexionmobile.botond.command.EnterKeyCommand;
import com.flexionmobile.botond.command.MaximizeWindowCommand;
import com.flexionmobile.botond.command.MouseClickCommand;
import com.flexionmobile.botond.command.MoveActiveWindowCommand;
import com.flexionmobile.botond.command.Registry;
import com.flexionmobile.botond.command.TypeCommand;
import com.flexionmobile.botond.command.WindowsKeyCommand;
import com.flexionmobile.botond.input.Keyboard;
import com.flexionmobile.botond.input.Mouse;
import com.flexionmobile.botond.parser.Action;
import com.flexionmobile.botond.parser.Script;
import com.flexionmobile.botond.parser.ScriptParser;

public class Main {

    private static final int SPEED = 300;

    public static void main(String[] args) throws Exception {

        if (args.length == 0) {
            throw new IllegalStateException("Usage: java -jar botond.jar [script]");
        }

        String scriptPath = args[0];

        File file = new File(scriptPath);
        Robot robot = new Robot();
        Mouse mouse = new Mouse(robot);
        Keyboard keyboard = new Keyboard(robot);
        Registry registry = getRegistry(robot, mouse, keyboard);
        ScriptParser scriptParser = new ScriptParser(registry);
        Script script = scriptParser.parse(file);
        execute(script);
    }

    private static void execute(Script script) throws InterruptedException {
        List<Action> actions = script.getActions();
        for (Action action : actions) {
            System.out.println("Executing " + action.getCommand().getClass().getSimpleName() + " " + Arrays.toString(action.getArgs()));
            Command command = action.getCommand();
            Thread.sleep(SPEED);
            command.execute(action.getArgs());
        }
    }

    private static Registry getRegistry(Robot robot, Mouse mouse, Keyboard keyboard) {
        Registry registry = new Registry();
        registry.register(new EnterKeyCommand(robot, mouse, keyboard));
        registry.register(new MaximizeWindowCommand(robot, mouse, keyboard));
        registry.register(new MouseClickCommand(robot, mouse, keyboard));
        registry.register(new MoveActiveWindowCommand(robot, mouse, keyboard));
        registry.register(new TypeCommand(robot, mouse, keyboard));
        registry.register(new WindowsKeyCommand(robot, mouse, keyboard));
        return registry;
    }
}
