package com.flexionmobile.botond.command;

import java.awt.*;

import com.flexionmobile.botond.input.Keyboard;
import com.flexionmobile.botond.input.Mouse;

public abstract class Command {

    static final int SPEED = 300;

    final Robot robot;
    final Mouse mouse;
    final Keyboard keyboard;

    public Command(Robot robot, Mouse mouse, Keyboard keyboard) {
        this.robot = robot;
        this.mouse = mouse;
        this.keyboard = keyboard;
    }

    public abstract void execute(String... args);

    public abstract String identifier();
}
