package com.flexionmobile.botond.command;

import java.awt.*;

import com.flexionmobile.botond.input.Keyboard;
import com.flexionmobile.botond.input.Mouse;

public class MouseClickCommand extends Command {

    public MouseClickCommand(Robot robot, Mouse mouse, Keyboard keyboard) {
        super(robot, mouse, keyboard);

    }

    @Override
    public void execute(String... args) {
        int x = Integer.parseInt(args[0]);
        int y = Integer.parseInt(args[1]);
        mouse.click(x, y);
    }

    @Override
    public String identifier() {
        return "click";
    }

}
