package com.flexionmobile.botond.command;

import java.awt.*;

import com.flexionmobile.botond.input.Keyboard;
import com.flexionmobile.botond.input.Mouse;

import static java.awt.event.InputEvent.BUTTON1_DOWN_MASK;
import static java.awt.event.KeyEvent.VK_ALT;
import static java.awt.event.KeyEvent.VK_M;
import static java.awt.event.KeyEvent.VK_SPACE;

public class MoveActiveWindowCommand extends Command {

    public MoveActiveWindowCommand(Robot robot, Mouse mouse, Keyboard keyboard) {
        super(robot, mouse, keyboard);
    }

    @Override
    public void execute(String... args) {
        int x = Integer.parseInt(args[0]);
        int y = Integer.parseInt(args[1]);

        // trigger move window menu
        robot.keyPress(VK_ALT);
        robot.keyPress(VK_SPACE);
        robot.keyRelease(VK_SPACE);
        robot.keyRelease(VK_ALT);
        robot.keyPress(VK_M);
        robot.keyRelease(VK_M);
        robot.delay(300);

        // move window
        robot.mousePress(BUTTON1_DOWN_MASK);
        robot.delay(100);
        robot.mouseMove(x, y);
        robot.delay(100);
        robot.mouseRelease(BUTTON1_DOWN_MASK);
    }

    @Override
    public String identifier() {
        return "moveActiveWindow";
    }
}
