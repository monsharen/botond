package com.flexionmobile.botond.command;

import java.awt.*;
import java.awt.event.KeyEvent;

import com.flexionmobile.botond.input.Keyboard;
import com.flexionmobile.botond.input.Mouse;

public class MaximizeWindowCommand extends Command {

    public MaximizeWindowCommand(Robot robot, Mouse mouse, Keyboard keyboard) {
        super(robot, mouse, keyboard);
    }

    @Override
    public void execute(String... args) {
        int[] maximizeIt = {KeyEvent.VK_ALT, KeyEvent.VK_SPACE, KeyEvent.VK_X};

        for (int i : maximizeIt) {
            robot.delay(SPEED);
            robot.keyPress(i);
            robot.keyRelease(i);
        }
    }

    @Override
    public String identifier() {
        return "maximizeWindow";
    }
}
