package com.flexionmobile.botond.command;

import java.util.HashMap;
import java.util.Map;

public class Registry {

    private final Map<String, Command> COMMANDS = new HashMap<>();

    public void register(Command command) {
        COMMANDS.put(command.identifier(), command);
    }

    public Command getCommand(String identifier) {
        Command command = COMMANDS.get(identifier);

        if (command != null) {
            return command;
        }

        throw new IllegalStateException("unknown command '" + identifier + "'");
    }
}
