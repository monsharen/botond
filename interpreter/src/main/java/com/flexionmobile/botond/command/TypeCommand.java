package com.flexionmobile.botond.command;

import java.awt.*;

import com.flexionmobile.botond.input.Keyboard;
import com.flexionmobile.botond.input.Mouse;

public class TypeCommand extends Command {

    public TypeCommand(Robot robot, Mouse mouse, Keyboard keyboard) {
        super(robot, mouse, keyboard);
    }

    @Override
    public void execute(String... args) {
        for (int i = 0; i < args.length; i++) {
            String text = args[i];
            keyboard.type(text);
            if (i < args.length) {
                keyboard.type(" ");
            }
        }
    }

    @Override
    public String identifier() {
        return "type";
    }
}
