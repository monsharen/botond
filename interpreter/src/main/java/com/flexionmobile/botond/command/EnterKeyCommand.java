package com.flexionmobile.botond.command;

import java.awt.*;
import java.awt.event.KeyEvent;

import com.flexionmobile.botond.input.Keyboard;
import com.flexionmobile.botond.input.Mouse;

public class EnterKeyCommand extends Command {

    public EnterKeyCommand(Robot robot, Mouse mouse, Keyboard keyboard) {
        super(robot, mouse, keyboard);
    }

    @Override
    public void execute(String... args) {
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }

    @Override
    public String identifier() {
        return "enterKey";
    }
}
