package com.flexionmobile.botond.command;

import java.awt.*;
import java.awt.event.KeyEvent;

import com.flexionmobile.botond.input.Keyboard;
import com.flexionmobile.botond.input.Mouse;

public class WindowsKeyCommand extends Command {


    public WindowsKeyCommand(Robot robot, Mouse mouse, Keyboard keyboard) {
        super(robot, mouse, keyboard);
    }

    @Override
    public void execute(String... args) {
        robot.keyPress(KeyEvent.VK_WINDOWS);
        robot.keyRelease(KeyEvent.VK_WINDOWS);
    }

    @Override
    public String identifier() {
        return "winKey";
    }
}
