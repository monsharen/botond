package com.flexionmobile.botond.parser;

import java.util.List;

import com.flexionmobile.botond.command.Command;

public class Script {

    private final List<Action> actions;

    public Script(List<Action> actions) {
        this.actions = actions;
    }

    public List<Action> getActions() {
        return actions;
    }
}
