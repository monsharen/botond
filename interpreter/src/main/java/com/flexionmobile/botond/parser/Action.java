package com.flexionmobile.botond.parser;

import com.flexionmobile.botond.command.Command;

public class Action {

    private final Command command;
    private final String[] args;

    public Action(Command command, String[] args) {
        this.command = command;
        this.args = args;
    }

    public Command getCommand() {
        return command;
    }

    public String[] getArgs() {
        return args;
    }
}
