package com.flexionmobile.botond.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.flexionmobile.botond.command.Command;
import com.flexionmobile.botond.command.Registry;

public class ScriptParser {

    private final Registry registry;

    public ScriptParser(Registry registry) {
        this.registry = registry;
    }

    public Script parse(File file) throws Exception {

        if (!file.exists() || !file.canRead()) {
            throw new IllegalStateException("can't open file " + file);
        }

        List<Action> actions = new ArrayList<Action>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.trim().length() == 0 || line.startsWith("#")) {
                    continue;
                }

                Action action = parse(line);
                actions.add(action);
            }
        }

        return new Script(actions);
    }

    private Action parse(String line) {
        String[] parts = line.split(" ");
        String identifier = parts[0];
        Command command = registry.getCommand(identifier);
        String[] args = Arrays.copyOfRange(parts, 1, parts.length);
        return new Action(command, args);
    }
}
