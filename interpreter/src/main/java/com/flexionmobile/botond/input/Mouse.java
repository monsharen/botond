package com.flexionmobile.botond.input;

import java.awt.*;
import java.awt.event.InputEvent;

public class Mouse {

    private final Robot robot;

    public Mouse(Robot robot) {
        this.robot = robot;
    }

    public void click(int x, int y) {
        int mask = InputEvent.BUTTON1_DOWN_MASK;
        robot.mouseMove(x, y);
        robot.mousePress(mask);
        robot.mouseRelease(mask);
    }
}
