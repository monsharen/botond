package com.flexionmobile.botond;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;

import javax.swing.*;

public class Sandbox {

    public static void main(String[] argv) throws Exception {
        /* JTextField component = new JTextField();
        component.addKeyListener(new MyKeyListener());

        JFrame f = new JFrame();

        f.add(component);
        f.setSize(300, 300);
        f.setVisible(true);
*/

        String[] test = new String[4];
        test[0] = "a";
        test[1] = "b";
        test[2] = "c";
        test[3] = "d";
        String[] strings = Arrays.copyOfRange(test, 1, test.length);
        for (String s : strings) {
            System.out.println(s);
        }

    }
}

class MyKeyListener extends KeyAdapter {
    public void keyPressed(KeyEvent evt) {


        System.out.println(evt.getKeyChar() + ", " + evt.getKeyCode() + ", " + evt.getExtendedKeyCode());
    }
}
